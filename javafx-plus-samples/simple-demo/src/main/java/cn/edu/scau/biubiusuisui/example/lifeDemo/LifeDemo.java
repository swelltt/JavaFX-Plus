package cn.edu.scau.biubiusuisui.example.lifeDemo;

import cn.edu.scau.biubiusuisui.annotation.FXScan;
import cn.edu.scau.biubiusuisui.config.FXPlusApplication;
import cn.edu.scau.biubiusuisui.log.FXPlusLoggerFactory;
import cn.edu.scau.biubiusuisui.log.IFXPlusLogger;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

import java.util.concurrent.TimeUnit;


/**
 * @author suisui
 * @version 1.2
 * @description 测试生命周期的Demo
 * @date 2020/5/1 11:50
 * @since JavaFX2.0 JDK1.8
 */
@FXScan(base = "cn.edu.scau.biubiusuisui.example.lifeDemo")
public class LifeDemo extends Application {
    private static IFXPlusLogger logger = FXPlusLoggerFactory.getLogger(LifeDemo.class);

    @Override
    public void start(Stage primaryStage) throws Exception {
        logger.info("LifeDemo---start");
        FXPlusApplication.start(getClass(), false);
        startThreadToStop();
    }

    /**
     * 睡眠10s后，关闭窗口，模拟后台任务完成后自动关闭窗口
     */
    private void startThreadToStop() {
        new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(10);
                logger.info("finish sleeping 10 seconds, ready to exit.");
                Platform.exit();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @Override
    public void init() throws Exception {
        logger.info("LifeDemo---init");
    }

    @Override
    public void stop() throws Exception {
        logger.info("LifeDemo---stop");
    }

}

