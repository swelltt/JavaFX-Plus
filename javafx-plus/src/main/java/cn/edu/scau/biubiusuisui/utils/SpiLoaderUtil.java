package cn.edu.scau.biubiusuisui.utils;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;

/**
 * SPI工具类
 *
 * @author Jade Yeung
 * @time 2022/4/30 08:55
 * @since 1.3.0
 */
public class SpiLoaderUtil {
    private SpiLoaderUtil() {
    }

    /**
     * 加载单个接口实例，如果有多个SPI实现类报错
     *
     * @param clazz 接口类型
     * @param <T>
     * @return
     */
    public static <T> T getService(Class<T> clazz) {
        return getService(clazz, Thread.currentThread().getContextClassLoader(), null);
    }

    /**
     * 加载单个接口实例，如果有多个SPI实现类报错
     *
     * @param clazz       接口类型
     * @param classLoader ClassLoader
     * @param <T>
     * @return
     */
    public static <T> T getService(Class<T> clazz, ClassLoader classLoader) {
        return getService(clazz, classLoader, null);
    }

    /**
     * 加载单个接口实例，如果有多个SPI实现类报错
     *
     * @param clazz      接口类型
     * @param defaultObj 默认对象
     * @param <T>
     * @return
     */
    public static <T> T getService(Class<T> clazz, T defaultObj) {
        return getService(clazz, Thread.currentThread().getContextClassLoader(), defaultObj);
    }

    /**
     * 加载单个接口实例，如果有多个SPI实现类报错
     *
     * @param clazz       接口类型
     * @param classLoader ClassLoader
     * @param defaultObj  默认对象
     * @param <T>
     * @return
     */
    public static <T> T getService(Class<T> clazz, ClassLoader classLoader, T defaultObj) {
        ServiceLoader<T> serviceLoader = ServiceLoader.load(clazz, classLoader);
        Iterator<T> iterator = serviceLoader.iterator();
        // 没有实现返回defaultObj
        if (!iterator.hasNext()) {
            return defaultObj;
        }
        T t = iterator.next();
        if (iterator.hasNext()) {
            throw new IllegalStateException("more than one SPI implement class");
        }
        return t;
    }

    /**
     * 加载所有接口实例
     *
     * @param clazz 接口类型
     * @param <T>   接口范型
     * @return 接口实例列表
     */
    public static <T> List<T> getServices(Class<T> clazz) {
        return getServices(clazz, Thread.currentThread().getContextClassLoader());
    }

    /**
     * 加载所有接口实例
     *
     * @param clazz       接口类型
     * @param classLoader ClassLoader
     * @param <T>         接口范型
     * @return 接口实例列表
     */
    public static <T> List<T> getServices(Class<T> clazz, ClassLoader classLoader) {
        List<T> services = new LinkedList<>();
        ServiceLoader<T> serviceLoader = ServiceLoader.load(clazz, classLoader);
        for (T t : serviceLoader) {
            services.add(t);
        }
        return services;
    }
}
