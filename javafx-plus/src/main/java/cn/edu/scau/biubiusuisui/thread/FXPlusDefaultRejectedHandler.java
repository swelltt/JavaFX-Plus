package cn.edu.scau.biubiusuisui.thread;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 拒绝策略
 *
 * @author Jade Yeung
 * @time 2022/4/30 19:52
 * @since 1.3.0
 */
public class FXPlusDefaultRejectedHandler implements RejectedExecutionHandler {
    private static final Logger logger = LoggerFactory.getLogger(FXPlusDefaultRejectedHandler.class);

    @Override
    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
        String threadName = "NoneThreadName";
        if (executor instanceof FXPlusExecutor) {
            threadName = ((FXPlusExecutor) executor).getThreadName();
            logger.error("threadPool={}, queueSize={}, coreSize={}, runnable={}", threadName, executor.getQueue().size(), executor.getCorePoolSize(), r);
        } else {
            logger.error("queueSize={}, coreSize={}, runnable={}", executor.getQueue().size(), executor.getCorePoolSize(), r);
        }
        throw new RejectedExecutionException(String.format("ThreadPool=%s reach the maximum of queue, reject new task!", threadName));
    }
}
