package cn.edu.scau.biubiusuisui.thread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * JavaFx-Plus线程池
 *
 * @author Jade Yeung
 * @time 2022/4/30 19:41
 * @since 1.3.0
 */
public class FXPlusExecutor extends ThreadPoolExecutor {
    private static final RejectedExecutionHandler defaultHandler = new FXPlusDefaultRejectedHandler();
    private String threadName = FXPlusExecutor.class.getName();

    public FXPlusExecutor(int coreSize, int maxSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(coreSize, maxSize, keepAliveTime, unit, workQueue, Executors.defaultThreadFactory(), defaultHandler);
    }

    public FXPlusExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, String threadName) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, Executors.defaultThreadFactory(), defaultHandler);
        this.threadName = threadName;
    }

    public FXPlusExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, boolean daemon, String threadName) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, new ThreadFactory() {
            @Override
            public Thread newThread(Runnable r) {
                Thread thread = new Thread(r);
                thread.setDaemon(daemon);
                thread.setName(threadName);
                return thread;
            }
        }, defaultHandler);
        this.threadName = threadName;
    }


    public String getThreadName() {
        return threadName;
    }

}
